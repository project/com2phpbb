﻿# $Id$

------------------------
COM2PHPBB MODULE README
------------------------
WARNING!

 This module is a variant of original COMMENT MODULE and fully replaces it for system.
 Before installing com2phpbb turn off and remove original COMMENT MODULE!

 This is a simple module to place comments to Drupal’s nodes in phpBB's database. It provides capability to start a discussion of node in Drupal and continue it both in Drupal and phpBB. The visitor of node will see full discussion. Module supplies a linking of users in databases in Drupal and phpBB.

------------------------
REQUIREMENTS
------------------------
1. This module requires Drupal 6 and removed original COMMENT MODULE. You must remove COMMENT MODULE before starting using this module.
2. This module has limited capability with Akismet.
3. This module requires placing Drupal's and phpBB's tables in same MySQL database with different prefixes.
4. The main ideology is to administer comments with the phpBB. So it’s a mistake to use modules for administration comments (COMMENT MOVER MODULE and so on).
5. phpBB does not allow use in posts HTML tags. So COM2PHPBB before storing comments replace HTML tags with bbcode tags and remove inconvertibility tags. So for normally displaying of comments from both systems you must add module for replacing bbcode tags with HTML tags (BBCODE module for example).

------------------------
INSTALLATION
------------------------
1. Extract the COM2PHPBB MODULE directory, including all its subdirectories, into Your sites/all/modules directory.
2. Turn off COMMENT MODULE and all modules that require it. You can make it on the Administer >> Site building >> Modules page.
3. Remove COMMENT MODULE from system. COMMENT MODULE usualy is placed in modules/comment directory.
3. Enable the COM2PHPBB MODULE on the Administer >> Site building >> Modules page.
   The database tables will be created automatically for you at this point.
4. Configure module on the Administer >> Content >> Comment >> Settings page.
5. Additional options can be set at Administer >> Content >> Comment >> Linked Users page.
6. If you are using CAPTCHA to protect from spam, you need add tag to comment's form of Com2phpBB: in http://example.com/admin/user/captcha/captcha/captcha_point add Form Id: com2phpbb_form

------------------------
AUTHOR / MAINTAINER
------------------------
Elia Reznik <elia@reznik.kiev.ua>

------------------------
WISH LIST
------------------------
To extend a functionality of module. I need an ordered “todo list”. Help me to build this “to-do list”.
