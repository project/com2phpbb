<?php

/**
 * @file
 * Admin page callbacks for the com2phpbb module.
 */

/**
 * Menu callback; present an administrative comment listing.
 */
function com2phpbb_admin($type = 'new') {
  $edit = $_POST;

  if (isset($edit['operation']) && ($edit['operation'] == 'delete') && isset($edit['comments']) && $edit['comments']) {
    return drupal_get_form('com2phpbb_multiple_delete_confirm');
  }
  else {
    return drupal_get_form('com2phpbb_admin_overview', $type, arg(4));
  }
}

/**
 * Form builder; Builds the comment overview form for the admin.
 *
 * @param $type
 *   Not used.
 * @param $arg
 *   Current path's fourth component deciding the form type (Published comments/Approval queue)
 * @return
 *   The form structure.
 * @ingroup forms
 * @see com2phpbb_admin_overview_validate()
 * @see com2phpbb_admin_overview_submit()
 * @see theme_com2phpbb_admin_overview()
 */
function com2phpbb_admin_overview($type = 'new', $arg) {
  // build an 'Update options' form
  $form['options'] = array(
    '#type' => 'fieldset', '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">', '#suffix' => '</div>'
  );
  $options = array();
  foreach (com2phpbb_operations($arg == 'approval' ? 'publish' : 'unpublish') as $key => $value) {
    $options[$key] = $value[0];
  }
  $form['options']['operation'] = array('#type' => 'select', '#options' => $options, '#default_value' => 'publish');
  $form['options']['submit'] = array('#type' => 'submit', '#value' => t('Update'));

  // load the comments that we want to display
  $status = ($arg == 'approval') ? COMMENT_NOT_PUBLISHED : COMMENT_PUBLISHED;
  $form['header'] = array('#type' => 'value', '#value' => array(
    theme('table_select_header_cell'),
    array('data' => t('Subject'), 'field' => 'subject'),
    array('data' => t('Author'), 'field' => 'name'),
    array('data' => t('Posted in'), 'field' => 'node_title'),
    array('data' => t('Time'), 'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Source'), 'field' => 'drcid')
  ));
  $result = pager_query('SELECT p.post_subject AS subject, phpbbt.dr_nid AS nid, p.post_id AS cid, p.post_text AS comment, p.post_time AS timestamp, p.post_approved AS status, p.post_username AS name, u.name AS registered_name, u.uid, c.cid AS drcid FROM {phpbb_thread} phpbbt, '._phpbb_prefix().'posts p LEFT JOIN {phpbb_users} phpbbu ON phpbbu.phpbb_userid = p.poster_id LEFT JOIN {users} u ON u.uid = phpbbu.dr_userid, '._phpbb_prefix().'posts p1 LEFT JOIN {comments} c ON c.cid = p1.post_id WHERE p.topic_id = phpbbt.phpbb_thread AND p.post_postcount > 1 AND  p.post_approved = %d AND p1.post_id = p.post_id'. tablesort_sql($form['header']['#value']), 50, 0, NULL, !$status);

  // build a table listing the appropriate comments
  $destination = drupal_get_destination();
  while ($comment = db_fetch_object($result)) {
    $comments[$comment->cid] = '';
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    $form['subject'][$comment->cid] = array('#value' => l($comment->subject, 'node/'. $comment->nid, array('attributes' => array('title' => truncate_utf8($comment->comment, 128)), 'fragment' => 'comment-'. $comment->cid)));
    $form['username'][$comment->cid] = array('#value' => theme('username', $comment));
    $form['node_title'][$comment->cid] = array('#value' => l($comment->node_title, 'node/'. $comment->nid));
    $form['timestamp'][$comment->cid] = array('#value' => format_date($comment->timestamp, 'small'));
	if ($comment->drcid > 0) {
    	$form['source'][$comment->cid] = array('#value' => t('Drupal'));
		}
		else {
		$form['source'][$comment->cid] = array('#value' => t('phpBB'));
		}
  }
  $form['comments'] = array('#type' => 'checkboxes', '#options' => isset($comments) ? $comments: array());
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}

/**
 * Validate com2phpbb_admin_overview form submissions.
 *
 * We can't execute any 'Update options' if no comments were selected.
 */
function com2phpbb_admin_overview_validate($form, &$form_state) {
  $form_state['values']['comments'] = array_diff($form_state['values']['comments'], array(0));
  if (count($form_state['values']['comments']) == 0) {
    form_set_error('', t('Please select one or more comments to perform the update on.'));
    drupal_goto('admin/content/comment');
  }
}

/**
 * Process com2phpbb_admin_overview form submissions.
 *
 * Execute the chosen 'Update option' on the selected comments, such as
 * publishing, unpublishing or deleting.
 */
function com2phpbb_admin_overview_submit($form, &$form_state) {
  $operations = com2phpbb_operations();
  if ($operations[$form_state['values']['operation']][1]) {
    // extract the appropriate database query operation
    $query = $operations[$form_state['values']['operation']][1];
    foreach ($form_state['values']['comments'] as $cid => $value) {
      if ($value) {
        // perform the update action, then refresh node statistics
        db_query($query, $cid);
        $comment = _com2phpbb_load($cid);
        _com2phpbb_update_node_statistics($comment->nid);
        // Allow modules to respond to the updating of a comment.
        com2phpbb_invoke_comment($comment, $form_state['values']['operation']);
        // Add an entry to the watchdog log.
        watchdog('content', 'Comment: updated %subject.', array('%subject' => $comment->subject), WATCHDOG_NOTICE, l(t('view'), 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)));
      }
    }
    cache_clear_all();
    drupal_set_message(t('The update has been performed.'));
    $form_state['redirect'] = 'admin/content/comment';
  }
}

/**
 * Theme the comment admin form.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @ingroup themeable
 */
function theme_com2phpbb_admin_overview($form) {
  $output = drupal_render($form['options']);
  if (isset($form['subject']) && is_array($form['subject'])) {
    foreach (element_children($form['subject']) as $key) {
      $row = array();
      $row[] = drupal_render($form['comments'][$key]);
      $row[] = drupal_render($form['subject'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      $row[] = drupal_render($form['node_title'][$key]);
      $row[] = drupal_render($form['timestamp'][$key]);
      $row[] = drupal_render($form['source'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No comments available.'), 'colspan' => '6'));
  }

  $output .= theme('table', $form['header']['#value'], $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function com2phpbb_admin_phpbbsettings() {
  $form['forum_settings'] = array(
    '#type' => 'fieldset',
	'#title' => t('phpBB\'s options'),
    '#collapsible' => TRUE,
  );
  
  $form['forum_settings']['com2phpbb_db_prefix'] = array(
    '#type' => 'textfield',
	'#title' => t('Database prefix for phpBB'),
	'#default_value' => variable_get('com2phpbb_db_prefix', COM2PHPBB_PREFIX),
	'#size' => 16,
	'#maxlength' => 16,
	'#description' => t('Prefix for table names'),
  );

  $form['forum_settings']['com2phpbb_forum_url'] = array(
    '#type' => 'textfield',
	'#title' => t('URL to phpBB\'s forum'),
	'#default_value' => variable_get('com2phpbb_forum_url', COM2PHPBB_FORUM_URL),
	'#description' => t('This URL will be used in links to forum\'s threads in nodes. [Example: URL http://www.example.com/forum/ will be used in building of link http://www.example.com/forum/showthread.php?t=124 to full discussion of node]. In node\'s settings you will be able to override this setting.'),
  );  
  $form['forum_settings']['com2phpbb_def_forum'] = array(
    '#type' => 'select',
	'#title' => t('Default forum to post comments'),
	'#default_value' => variable_get('com2phpbb_def_forum', COM2PHPBB_FORUM_POST),
	'#options' => _phpbb_forum_list(),
	'#size' => 5,
	'#maxlength' => 16,
	'#multiple' => FALSE,
  );  

  $form['forum_settings']['com2phpbb_html_conv'] = array(
    '#type' => 'checkbox',
	'#title' => t('Convert HTML tags in comments to BBCode'),
	'#default_value' => variable_get('com2phpbb_html_conv', COM2PHPBB_HTML_CONV),
  );

  $form['create_settings'] = array(
    '#type' => 'fieldset',
	'#title' => t('Create settings'),
    '#collapsible' => TRUE,
  );

   $form['create_settings']['com2phpbb_teaser_author'] = array(
    '#type' => 'checkbox',
	'#title' => t('Author of first comment is an author of start post in forum. (If unchecked the author of node will be the author of start post)'),
	'#default_value' => variable_get('com2phpbb_teaser_author', COM2PHPBB_TEASER_AUTHOR),
  );
    
  $form['create_settings']['com2phpbb_reply_resubj'] = array(
    '#type' => 'checkbox',
	'#title' => t('Generate subject Re: for reply'),
	'#default_value' => variable_get('com2phpbb_reply_resubj', COM2PHPBB_REPLY_RESUBJ),
  );

  $form['create_settings']['com2phpbb_reply_autoquote'] = array(
    '#type' => 'checkbox',
	'#title' => t('Generate quoted content of node or comment for reply'),
	'#default_value' => variable_get('com2phpbb_reply_autoquote', COM2PHPBB_REPLY_AUTOQUOTE),
  );

  return system_settings_form($form);
}

/**
 * Menu callback; presents the linked users settings page.
 */
function com2phpbb_admin_linkedusers() {
  $form['users_list'] = array(
    '#type' => 'fieldset',
	'#title' => t('Crossed users'),
	'#description' => t('List of linked users, who have accounts in Drupal and phpBB. To add a new linked user click ' . l(t('here'), 'admin/content/comment/linkedusers/add')),
    '#collapsible' => TRUE,
  );

$form['header'] = array('#type' => 'value', '#value' => array(
	theme('table_select_header_cell'),
    array('data' => t('Drupal Id'), 'field' => 'dr_userid', 'sort' => 'asc'),
    array('data' => t('Drupal name'), 'field' => 'dr_name'),
    array('data' => t('phpBB Id'), 'field' => 'phpbb_userid'),
    array('data' => t('phpBB name'), 'field' => 'phpbb_name')
  ));  

	$result =  db_query('SELECT dru.uid AS dr_userid, dru.name AS dr_name, phpbbu.user_id AS phpbb_userid, phpbbu.username AS phpbb_name FROM {users} dru, {phpbb_users} drphpbb, '._phpbb_prefix().'users phpbbu WHERE drphpbb.dr_userid = dru.uid AND phpbbu.user_id = drphpbb.phpbb_userid '. tablesort_sql($form['header']['#value']));

while ($user = db_fetch_object($result)) {
	$users[$user->dr_userid] = '';
	$form['dr_userid'][$user->dr_userid] = array('#value' => $user->dr_userid);
	$form['dr_name'][$user->dr_userid] = array('#value' => $user->dr_name);
	$form['phpbb_userid'][$user->dr_userid] = array('#value' => $user->phpbb_userid);
	$form['phpbb_name'][$user->dr_userid] = array('#value' => $user->phpbb_name); 
	}
	$form['users'] = array('#type' => 'checkboxes', '#options' => $users);
	
  $form['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Delete selected users'),
	);
	
	return $form;
}
/**
 * Menu callback; presents the threads in forum with comments from site.
 */
function com2phpbb_admin_threads() {
  $form['thread_list'] = array(
    '#type' => 'fieldset',
	'#title' => t('Linked threads'),
	'#description' => t('Below is a list of linked threads in forum with nodes from site. You can unlink it from nodes. Unlinking threads with nodes does not delete anything - node become uncommented and thread in forum become "independent" from node.'),
    '#collapsible' => TRUE,
  );

$form['header'] = array('#type' => 'value', '#value' => array(
	theme('table_select_header_cell'),
    array('data' => t('Title'), 'field' => 'nid'),
    array('data' => t('Published'), 'field' => 'drtime', 'sort' => 'desc'),
    array('data' => t('phpbb thread'), 'field' => 'topic_id'),
    array('data' => t('Total comments'), 'field' => 'replycount'),
	array('data' => t('Last comment'), 'field' => 'lastpost'),
  ));  

	$result =  db_query('SELECT n.nid, n.title AS drtitle, n.changed AS drtime, frt.topic_id, frt.topic_title AS phpbbtitle, frt.topic_replies, frt.topic_last_post_time FROM {node} n LEFT JOIN {phpbb_thread} phpbbt ON phpbbt.dr_nid = n.nid LEFT JOIN '._phpbb_prefix().'topics frt ON frt.topic_id = phpbbt.phpbb_thread WHERE phpbbt.dr_nid > 0 AND n.status = 1 '. tablesort_sql($form['header']['#value']));

while ($thread = db_fetch_object($result)) {
	$threads[$thread->nid] = '';
	$form['node'][$thread->nid] = array('#value' => l($thread->drtitle, 'node/'. $thread->nid));
	$form['node_time'][$thread->nid] = array('#value' => format_date($thread->drtime, 'small'));
	$form['thread'][$thread->nid] = array('#value' => l($thread->phpbbtitle, variable_get('com2phpbb_forum_url', COM2PHPBB_FORUM_URL).'viewtopic.php?t='. $thread->threadid));
	$form['thread_count'][$thread->nid] = array('#value' => $thread->topic_replies); 
	$form['thread_lastpost'][$thread->nid] = array('#value' => format_date($thread->topic_last_post_time, 'small'));
	}
	$form['threads'] = array('#type' => 'checkboxes', '#options' => $threads);
	
  $form['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Unlink selected threads'),
	);
	
	return $form;
}
/**
 * We can't delete linked users if no users were selected.
 */
function com2phpbb_admin_linkedusers_validate($form_id, $form_values) {
  $form_values['users'] = array_diff($form_values['users'], array(0));
  if (count($form_values['users']) == 0) {
    form_set_error('', t('Please select one or more users to delete.'));
    drupal_goto('admin/content/comment/linkedusers');
  }
}
/**
 * We can't unlink threads if no threads were selected.
 */
function com2phpbb_admin_threads_validate($form_id, $form_values) {
  $form_values['values']['threads'] = array_diff($form_values['values']['threads'], array(0));
  if (count($form_values['values']['threads']) == 0) {
    form_set_error('', t('Please select one or more threads to unlink.'));
    drupal_goto('admin/content/comment/threads');
  }
}
/**
 * Delete selected linked users from cross table
 */
function com2phpbb_admin_linkedusers_submit($form_id, $form_values) {

    foreach ($form_values['users'] as $dr_userid => $value) {
      if ($value) {
        // perform the update action, then refresh node statistics
        db_query('DELETE FROM {phpbb_users} WHERE dr_userid = %d', $dr_userid);
      }
    }
    cache_clear_all();
    drupal_set_message(t('Cross users has been deleted.'));
    return 'admin/content/comment/linkedusers';

}
/**
 * Delete selected linked users from cross table
 */
function com2phpbb_admin_threads_submit($form_id, $form_values) {

    foreach ($form_values['values']['threads'] as $dr_nid=>$value) {
      if ($value) {
        db_query('DELETE FROM {phpbb_thread} WHERE dr_nid = %d', $dr_nid);
      }
    }
    cache_clear_all();
    drupal_set_message(t('Threads has been unlinked.'));
    return 'admin/content/comment/threads';

}

function theme_com2phpbb_admin_linkedusers($form) {	
$output = drupal_render($form['users_list']);

if (isset($form['dr_userid']) && is_array($form['dr_userid'])) {
	foreach (element_children($form['dr_userid']) as $key) {
      $row = array();
	  $row[] = drupal_render($form['users'][$key]);
      $row[] = drupal_render($form['dr_userid'][$key]);
      $row[] = drupal_render($form['dr_name'][$key]);
      $row[] = drupal_render($form['phpbb_userid'][$key]);
      $row[] = drupal_render($form['phpbb_name'][$key]);
	  $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No linked users available.'), 'colspan' => '5'));
  }

	$output .= theme('table', $form['header']['#value'], $rows);
	$output .= drupal_render($form);
	
	return $output;
}

function theme_com2phpbb_admin_threads($form) {	
$output = drupal_render($form['thread_list']);

if (isset($form['node']) && is_array($form['node'])) {
	foreach (element_children($form['node']) as $key) {
      $row = array();
	  $row[] = drupal_render($form['threads'][$key]);
	  $row[] = drupal_render($form['node'][$key]);
      $row[] = drupal_render($form['node_time'][$key]);
      $row[] = drupal_render($form['thread'][$key]);
      $row[] = drupal_render($form['thread_count'][$key]);
      $row[] = drupal_render($form['thread_lastpost'][$key]);
	  $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No linked threads available.'), 'colspan' => '5'));
  }

	$output .= theme('table', $form['header']['#value'], $rows);
	$output .= drupal_render($form);
	
	return $output;
}
/**
 * Menu callback; add linked users page.
 */
function com2phpbb_admin_linkedusers_new() {
  $form['users_new'] = array(
    '#type' => 'fieldset',
	'#title' => t('Add new linked user'),
    '#collapsible' => FALSE,
  ); 
  
  $dr_user = array();
  $dr_users = db_query('SELECT dr.uid, dr.name FROM {users} dr LEFT JOIN {phpbb_users} phpbbu ON phpbbu.dr_userid = dr.uid WHERE phpbbu.dr_userid IS NULL AND dr.uid > 0 ORDER BY dr.name');
  while ($user1 = db_fetch_object($dr_users)) {
  	$dr_user[$user1->uid] = $user1->name;
	}
  
  $form['users_new']['dr_user'] = array(
  	'#type' => 'select',
	'#title' => t('User from Drupal'),
	'#options' => $dr_user
	);

  $phpbb_user = array();
  $phpbb_users = db_query('SELECT fr.user_id, fr.username FROM '._phpbb_prefix().'users fr LEFT JOIN {phpbb_users} phpbbu ON phpbbu.phpbb_userid = fr.user_id WHERE phpbbu.phpbb_userid IS NULL ORDER BY fr.username');
  while ($user2 = db_fetch_object($phpbb_users)) {
  	$phpbb_user[$user2->user_id] = $user2->username;
	}
  
  $form['users_new']['phpbb_user'] = array(
  	'#type' => 'select',
	'#title' => t('User from phpBB'),
	'#options' => $phpbb_user
	);
 
   $form['users_new']['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Add new linked user'),
	);

  return $form;
}

function com2phpbb_admin_linkedusers_new_submit($form_id, $form_values) {

	$dr_cross = $form_values['values']['dr_user'];
	$phpbb_cross = $form_values['values']['phpbb_user'];
	
	if ($dr_cross && $phpbb_cross) {
		db_query('INSERT INTO {phpbb_users}(dr_userid, phpbb_userid) VALUES(%d, %d)', $dr_cross, $phpbb_cross);
		}
		
	return 'admin/content/comment/linkedusers';
}

/**
 * List the selected comments and verify that the admin really wants to delete
 * them.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @return
 *   TRUE if the comments should be deleted, FALSE otherwise.
 * @ingroup forms
 * @see comment_multiple_delete_confirm_submit()
 */
function com2phpbb_multiple_delete_confirm(&$form_state) {
  $edit = $form_state['post'];

  $form['comments'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter() returns only elements with actual values
  $comment_counter = 0;
  foreach (array_filter($edit['comments']) as $cid => $value) {
    $comment = _com2phpbb_load($cid);
    if (is_object($comment) && is_numeric($comment->cid)) {
      $subject = db_result(db_query('SELECT post_subject AS subject FROM '._phpbb_prefix().'posts WHERE post_id = %d', $cid));
      $form['comments'][$cid] = array('#type' => 'hidden', '#value' => $cid, '#prefix' => '<li>', '#suffix' => check_plain($subject) .'</li>');
      $comment_counter++;
    }
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  if (!$comment_counter) {
    drupal_set_message(t('There do not appear to be any comments to delete or your selected comment was deleted by another administrator.'));
    drupal_goto('admin/content/comment');
  }
  else {
    return confirm_form($form,
                        t('Are you sure you want to delete these comments and all their children?'),
                        'admin/content/comment', t('This action cannot be undone.'),
                        t('Delete comments'), t('Cancel'));
  }
}

/**
 * Process com2phpbb_multiple_delete_confirm form submissions.
 *
 * Perform the actual comment deletion.
 */
function com2phpbb_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['comments'] as $cid => $value) {
      $comment = _com2phpbb_load($cid);
      _com2phpbb_delete_thread($comment);
      _com2phpbb_update_node_statistics($comment->nid);
    }
    cache_clear_all();
    drupal_set_message(t('The comments have been deleted.'));
  }
  $form_state['redirect'] = 'admin/content/comment';
}

/**
 * Menu callback; delete a comment.
 *
 * @param $cid
 *   The comment do be deleted.
 */
function com2phpbb_delete($cid = NULL) {
  $comment = db_fetch_object(db_query('SELECT c.*, u.name AS registered_name, u.uid FROM {comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE c.cid = %d', $cid));
  $comment->name = $comment->uid ? $comment->registered_name : $comment->name;

  $output = '';

  if (is_object($comment) && is_numeric($comment->cid)) {
    $output = drupal_get_form('com2phpbb_confirm_delete', $comment);
  }
  else {
    drupal_set_message(t('The comment no longer exists.'));
  }

  return $output;
}

/**
 * Form builder; Builds the confirmation form for deleting a single comment.
 *
 * @ingroup forms
 * @see comment_confirm_delete_submit()
 */
function com2phpbb_confirm_delete(&$form_state, $comment) {
  $form = array();
  $form['#comment'] = $comment;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the comment %title?', array('%title' => $comment->subject)),
    'node/'. $comment->nid,
    t('Any replies to this comment will be lost. This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'comment_confirm_delete');
}

/**
 * Process com2phpbb_confirm_delete form submissions.
 */
function com2phpbb_confirm_delete_submit($form, &$form_state) {
  drupal_set_message(t('The comment and all its replies have been deleted.'));

  $comment = $form['#comment'];

  // Delete comment and its replies.
  _com2phpbb_delete_thread($comment);

  _com2phpbb_update_node_statistics($comment->nid);

  // Clear the cache so an anonymous user sees that his comment was deleted.
  cache_clear_all();

  $form_state['redirect'] = "node/$comment->nid";
}

/**
 * Perform the actual deletion of a comment and all its replies.
 *
 * @param $comment
 *   An associative array describing the comment to be deleted.
 */
function _com2phpbb_delete_thread($comment) {
  if (!is_object($comment) || !is_numeric($comment->cid)) {
    watchdog('content', 'Cannot delete non-existent comment.', array(), WATCHDOG_WARNING);
    return;
  }

  // Delete the comment:
  db_query('DELETE FROM {comments} WHERE cid = %d', $comment->cid);
  db_query('DELETE FROM '._phpbb_prefix().'posts WHERE post_id = %d', $comment->cid);
  watchdog('content', 'Comment: deleted %subject.', array('%subject' => $comment->subject));

  com2phpbb_invoke_comment($comment, 'delete');

  // Delete the comment's replies
  $result = db_query('SELECT c.*, u.name AS registered_name, u.uid FROM {comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE pid = %d', $comment->cid);
  while ($comment = db_fetch_object($result)) {
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    _com2phpbb_delete_thread($comment);
  }
}
